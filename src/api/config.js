import axios from 'axios';

export const marvelAxios = axios.create({
  baseURL: 'https://gateway.marvel.com:443/v1/public/',
  params:  {
    apikey:  process.env.REACT_APP_MARVEL_API_KEY
  },
  timeout: 10000
});
