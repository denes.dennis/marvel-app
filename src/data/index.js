import { combineReducers } from 'redux';
import { comics }          from "../pages/ComicBookList/data/reducers";
import { comic }           from "../pages/ComicBookProfile/data/reducers";

export default combineReducers({
  comics,
  comic
});
