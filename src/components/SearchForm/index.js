import React     from 'react';
import PropTypes from 'prop-types';

export class SearchForm extends React.Component {
  state = { term: "" };

  render() {
    return (
      <div className="form-group my-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search comic book..."
          value={ this.state.term }
          onChange={ event => this.onInputChange(event.target.value) }
        />
      </div>
    );
  }

  onInputChange(term) {
    this.setState({ term });
    this.props.onSearchTermChange(term);
  }
}

SearchForm.propTypes = {
  onSearchTermChange: PropTypes.func.isRequired
};
