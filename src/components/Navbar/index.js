import React        from 'react';
import PropTypes    from 'prop-types';
import { Row, Col } from "../../components/external/Grid";
import Logo         from '../../assets/logo.png';

export class Navbar extends React.Component {
  render() {
    const { brand } = this.props;
    return (
      <nav className="navbar navbar-expand-lg bg-primary">
        <div className="container">
          <Row style={ { flexGrow: '1' } }>
            <Col md={ 3 }>
              <a className="navbar-brand" href="/">
                <img src={ Logo } height="60" alt="logo" style={{backgroundColor: 'white'}}/>
              </a>
            </Col>
          </Row>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  brand: PropTypes.string
};

Navbar.defaultProps = {
  brand: "Brand"
};


