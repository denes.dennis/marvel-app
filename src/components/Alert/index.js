import React     from 'react';
import PropTypes from 'prop-types';
import ExtAlert  from 'react-bootstrap/Alert'

export const Alert = ({ type, title, message }) => {
  return (
    <ExtAlert variant={ type }>
      <ExtAlert.Heading>{ title }</ExtAlert.Heading>
      <p>{ message }</p>
    </ExtAlert>
  );
};

Alert.propTypes = {
  type:    PropTypes.string.isRequired,
  title:   PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
};

