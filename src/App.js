import React                    from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import { Container }            from "./components/external/Grid";
import { Navbar }               from './components/Navbar';
import { ComicBookProfile }     from "./pages/ComicBookProfile";
import { HomePage }             from "./pages/Homepage";

export class App extends React.Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <>
            <Navbar/>
            <Container>
              <Route exact path="/" component={ HomePage }/>
              <Route path="/comic/:id" component={ ComicBookProfile }/>
            </Container>
          </>
        </BrowserRouter>
      </div>
    );
  }
}

