import React         from 'react';
import { shallow }   from 'enzyme';
import { HomePage }  from './index';

const props = {
  getComics: () => {
    return Promise.resolve()
  }
};

const setup = props => {
  return shallow(<HomePage.WrappedComponent { ...props } />);
};

describe('HomePage component tests', () => {
  it('renders without crashing', () => {
    setup(props)
  });

  it('renders correct information', () => {
    const wrapper = setup(props);
    expect(wrapper.find('SearchForm').length).toBe(1);
    expect(wrapper.find('Connect(ComicBookListClass)').length).toBe(1);
  })
});
