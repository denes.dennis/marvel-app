import React             from 'react';
import { connect }       from 'react-redux';
import _                 from "lodash";
import { ComicBookList } from "../../pages/ComicBookList";
import { SearchForm }    from "../../components/SearchForm";
import { getComics }     from "../../pages/ComicBookList/data/actions/comics";

class HomePageClass extends React.Component {
  constructor(props) {
    super(props);
    this.comicSearch("a");
  }

  render() {
    const comicSearch = _.debounce(term => {
      this.comicSearch(term);
    }, 300);

    return (
      <div className="Home">
        <SearchForm onSearchTermChange={ comicSearch }/>
        <ComicBookList/>
      </div>
    );
  }

  comicSearch(term) {
    this.props.getComics({ titleStartsWith: term })
  }
}

export const HomePage = connect(null, { getComics })(HomePageClass);
