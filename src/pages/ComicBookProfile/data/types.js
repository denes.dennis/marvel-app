export const GET_COMIC_LOADING = 'get_comic_loading';
export const GET_COMIC_SUCCESS = 'get_comic_success';
export const GET_COMIC_ERROR   = 'get_comic_error';
