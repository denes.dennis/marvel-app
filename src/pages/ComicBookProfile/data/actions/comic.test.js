import {
  getComicLoading,
  getComicSuccess,
  getComicError
} from './comic';
import {
  GET_COMIC_LOADING,
  GET_COMIC_SUCCESS,
  GET_COMIC_ERROR
} from "../types";

describe('Comic Actions', () => {
  it('should create a GET_COMIC_LOADING action', () => {
    const expectedAction = { type: GET_COMIC_LOADING };
    const action         = getComicLoading();
    expect(action).toEqual(expectedAction);
  });

  it('should create a GET_COMIC_ERROR action with error', () => {
    const expectedAction = { type: GET_COMIC_ERROR, payload: 'error' };
    const action         = getComicError('error');
    expect(action).toEqual(expectedAction);
  });

  it('should create a GET_COMIC_SUCCESS action', () => {
    const comic = {
      title:       'Amazing Spider-Man 500',
      description: 'Lorem Ipsum',
      thumbnail:   {
        path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
        extension: 'jpg'
      },
      characters:  {
        available:     0,
        collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
        items:         [],
        returned:      0
      },
      events:      {
        available:     0,
        collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
        items:         [],
        returned:      0
      },
      stories:     {
        available:     0,
        collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
        items:         [ {
          resourceURI: "http://gateway.marvel.com/v1/public/stories/351",
          name:        "Interior #351",
          type:        "interiorStory"
        } ],
        returned:      0
      }
    };

    const expectedAction = {
      type:    GET_COMIC_SUCCESS,
      payload: {
        title:       'Amazing Spider-Man 500',
        description: 'Lorem Ipsum',
        thumbnail:   {
          path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg'
        },
        characters:  {
          available:     0,
          collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
          items:         [],
          returned:      0
        },
        events:      {
          available:     0,
          collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
          items:         [],
          returned:      0
        },
        stories:     {
          available:     0,
          collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
          items:         [ {
            resourceURI: "http://gateway.marvel.com/v1/public/stories/351",
            name:        "Interior #351",
            type:        "interiorStory"
          } ],
          returned:      0
        }
      }
    };
    const action         = getComicSuccess(comic);
    expect(action).toEqual(expectedAction);
  })
});
