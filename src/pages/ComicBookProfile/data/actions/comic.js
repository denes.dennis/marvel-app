import { marvelAxios } from "../../../../api/config";
import { MyError }     from "../../../../utils/error";
import {
  GET_COMIC_LOADING,
  GET_COMIC_SUCCESS,
  GET_COMIC_ERROR
}                      from "../types";

export const getComicLoading = () => {
  return {
    type: GET_COMIC_LOADING
  }
};

export const getComicSuccess = (comics) => {
  return {
    type:    GET_COMIC_SUCCESS,
    payload: comics
  }
};

export const getComicError = (error) => {
  return {
    type:    GET_COMIC_ERROR,
    payload: error
  }
};

export const getComic = (comicId) => {
  return async (dispatch) => {
    try {
      dispatch(getComicLoading());
      const res = await marvelAxios.get(`/comics/${ comicId }`);
      dispatch(getComicSuccess(res));
    } catch (error) {
      dispatch(getComicError(error.response));
      MyError(error);
    }
  };
};
