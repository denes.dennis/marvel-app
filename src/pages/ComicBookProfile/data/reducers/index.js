import {
  GET_COMIC_LOADING,
  GET_COMIC_SUCCESS,
  GET_COMIC_ERROR
} from "../types";

const initialState = {
  comicLoading: false,
  comicError:   "",
  data:          []
};

export const comic = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_COMIC_LOADING:
      return {
        ...state,
        comicLoading: true
      };
    case GET_COMIC_SUCCESS:
      return {
        ...state,
        comicLoading: false,
        data:          payload.data.data
      };
    case GET_COMIC_ERROR:
      return {
        ...state,
        comicLoading: false,
        comicError:   payload.data.message
      };
    default:
      return state;
  }
};
