import { comic } from "./index";
import {
  GET_COMIC_LOADING,
  GET_COMIC_SUCCESS,
  GET_COMIC_ERROR
}                from "../types";

describe('Comic Reducers', () => {
  it('should return the initial state', () => {
    expect(comic(undefined, {})).toEqual({
      comicLoading: false,
      comicError:   "",
      data:         []
    })
  });

  it('should handle GET_COMIC_LOADING', () => {
    expect(
      comic(
        {
          comicLoading: false,
          comicError:   "",
          data:         []
        },
        {
          type: GET_COMIC_LOADING
        }
      )
    ).toEqual({
      comicLoading: true,
      comicError:   "",
      data:         []
    })
  });

  it('should handle GET_COMIC_ERROR', () => {
    expect(
      comic(
        {
          comicLoading: false,
          comicError:   "",
          data:         []
        },
        {
          type:    GET_COMIC_ERROR,
          payload: { data: { message: 'errorOps' } }
        }
      )
    ).toEqual({
      comicLoading: false,
      comicError:   "errorOps",
      data:         []
    })
  });

  it('should handle GET_COMIC_SUCCESS', () => {
    expect(
      comic(
        {
          comicLoading: false,
          comicError:   "",
          data:         []
        },
        {
          type:    GET_COMIC_SUCCESS,
          payload: {
            data: {
              data: {
                title:       'Amazing Spider-Man 500',
                description: 'Lorem Ipsum',
                thumbnail:   {
                  path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
                  extension: 'jpg'
                },
                characters:  {
                  available:     0,
                  collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
                  items:         [],
                  returned:      0
                },
                events:      {
                  available:     0,
                  collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
                  items:         [],
                  returned:      0
                },
                stories:     {
                  available:     0,
                  collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
                  items:         [ {
                    resourceURI: "http://gateway.marvel.com/v1/public/stories/351",
                    name:        "Interior #351",
                    type:        "interiorStory"
                  } ],
                  returned:      0
                }
              }
            }
          }
        }
      )
    ).toEqual({
      comicLoading: false,
      comicError:   "",
      data:         {
        title:       'Amazing Spider-Man 500',
        description: 'Lorem Ipsum',
        thumbnail:   {
          path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg'
        },
        characters:  {
          available:     0,
          collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
          items:         [],
          returned:      0
        },
        events:      {
          available:     0,
          collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
          items:         [],
          returned:      0
        },
        stories:     {
          available:     0,
          collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
          items:         [ {
            resourceURI: "http://gateway.marvel.com/v1/public/stories/351",
            name:        "Interior #351",
            type:        "interiorStory"
          } ],
          returned:      0
        }
      }
    })
  });
});
