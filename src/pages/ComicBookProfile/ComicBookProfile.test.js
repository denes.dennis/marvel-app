import React                from 'react';
import { shallow }          from 'enzyme';
import { ComicBookProfile } from './index';

const props = {
  getComic: () => {
    return Promise.resolve()
  },
  match: {
    params: {
      id: 12
    }
  },
  comic: {
    data: {
      results: [
        {
          title:       'Amazing Spider-Man 500',
          description: 'Lorem Ipsum',
          thumbnail:   {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          },
          characters:  {
            available:     0,
            collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
            items:         [],
            returned:      0
          },
          events:      {
            available:     0,
            collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
            items:         [],
            returned:      0
          },
          stories:     {
            available:     0,
            collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
            items:         [ {
              resourceURI: "http://gateway.marvel.com/v1/public/stories/351",
              name:        "Interior #351",
              type:        "interiorStory"
            } ],
            returned:      0
          }
        }
      ]
    }
  }
};

const props2 = {
  getComic: () => {
    return Promise.resolve()
  },
  match: {
    params: {
      id: 12
    }
  },
  comic: {
    data:        {},
    comicError: 'OleOle'
  }
};

const props3 = {
  getComic: () => {
    return Promise.resolve()
  },
  match: {
    params: {
      id: 12
    }
  },
  comic: {}
};

const setup = props => {
  return shallow(<ComicBookProfile.WrappedComponent { ...props } />);
};

describe('ComicBookProfile component tests', () => {
  it('renders without crashing', () => {
    setup(props);
  });

  it('renders correct information', () => {
    const wrapper = setup(props);
    expect(wrapper.find('ComicProfileCard').length).toBe(1);
    expect(wrapper.find('ComicProfileCard').props().comic).toEqual(props.comic.data.results[0]);
  });

  it('displays error if there is an error fetching the comic profile', () => {
    const wrapper = setup(props2);
    expect(wrapper.find('Alert').length).toBe(1);
    expect(wrapper.find('Alert').props().message).toEqual(props2.comic.comicError);
  });

  it('displays warning if there is no comic book', () => {
    const wrapper = setup(props3);
    expect(wrapper.find('Alert').length).toBe(1);
    expect(wrapper.find('Alert').props().message).toEqual('No comic book found.');
  })
});
