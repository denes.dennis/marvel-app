import React                from 'react';
import { connect }          from "react-redux";
import { getComic }         from "./data/actions/comic";
import { Spinner }          from "../../components/Spinner";
import { Alert }            from "../../components/Alert";
import { ComicProfileCard } from "./components/ComicProfileCard";

class ComicBookProfileClass extends React.Component {
  componentDidMount() {
    this.props.getComic(this.props.match.params.id);
  }

  render() {
    const { comic: { comicLoading } } = this.props;

    return comicLoading ? <Spinner/> : this.displayComicProfile();
  }

  displayComicProfile() {
    const { comic: { comicError, data } } = this.props;

    if ( data && data.results && data.results.length ) {
      return <ComicProfileCard comic={ data.results[ 0 ] }/>;
    } else if ( comicError ) {
      return <Alert type="danger" title="Error" message={ comicError }/>;
    } else {
      return <Alert type="warning" title=":(" message="No comic book found."/>;
    }
  }
}

const mapStateToProps = ({ comic }) => {
  return { comic }
};

export const ComicBookProfile = connect(mapStateToProps, { getComic })(ComicBookProfileClass);
