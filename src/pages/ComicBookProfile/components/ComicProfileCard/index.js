import React                   from 'react';
import PropTypes               from 'prop-types';
import { Row, Col }            from "../../../../components/external/Grid";
import { Link }                from "react-router-dom";
import { ComicProfileSection } from "../ComicProfileSection";

export class ComicProfileCard extends React.Component {
  render() {
    const { comic } = this.props;
    return (
      <>
        <Row className="my-2">
          <Col xs={ 12 }>
            <Link to="/"><span style={ { textDecoration: 'underline' } }>Back</span></Link>
          </Col>
        </Row>

        <Row>
          <Col md={ 4 }>
            <img
              src={ `${ comic.thumbnail.path }.${ comic.thumbnail.extension }` }
              height="300px"
              alt={ comic.title }
              style={ { objectFit: 'cover' } }
            />
          </Col>
          <Col md={ 8 }>
            <h1 className="comic-title">{ comic.title }</h1>
            <p className="comic-description">{ comic.description }</p>
          </Col>
        </Row>

        <Row>
          <Col>
            <ComicProfileSection title="Characters" data={comic.characters} />
          </Col>
        </Row>

        <Row>
          <Col>
            <ComicProfileSection title="Events" data={comic.events} />
          </Col>
        </Row>

        <Row>
          <Col>
            <ComicProfileSection title="Stories" data={comic.stories} />
          </Col>
        </Row>
      </>
    );
  }
}

ComicProfileCard.propTypes = {
  comic: PropTypes.object.isRequired
};
