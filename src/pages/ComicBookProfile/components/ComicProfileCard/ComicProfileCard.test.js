import React                from 'react'
import { shallow }          from 'enzyme'
import { ComicProfileCard } from './index'

const setup = props => {
  return shallow(<ComicProfileCard { ...props } />)
};

const props = {
  comic: {
    title:       'Amazing Spider-Man 500',
    description: 'Lorem Ipsum',
    thumbnail:   {
      path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
      extension: 'jpg'
    },
    characters:  {
      available:     0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
      items:         [],
      returned:      0
    },
    events:      {
      available:     0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
      items:         [],
      returned:      0
    },
    stories:     {
      available:     0,
      collectionURI: "http://gateway.marvel.com/v1/public/comics/1220/characters",
      items:         [ {
        resourceURI: "http://gateway.marvel.com/v1/public/stories/351",
        name:        "Interior #351",
        type:        "interiorStory"
      } ],
      returned:      0
    }
  }
};

describe('ComicProfileCard component tests', () => {
  it('renders without crashing', () => {
    setup(props);
  });

  it('renders correct information', () => {
    const wrapper = setup(props);
    expect(wrapper.find('img').props().src).toEqual(`${ props.comic.thumbnail.path }.${ props.comic.thumbnail.extension }`);
    expect(wrapper.find('.comic-title').text()).toEqual(props.comic.title);
    expect(wrapper.find('.comic-description').text()).toEqual(props.comic.description);
    expect(wrapper.find('ComicProfileSection[title="Characters"]').props().data).toEqual(props.comic.characters);
    expect(wrapper.find('ComicProfileSection[title="Events"]').props().data).toEqual(props.comic.events);
    expect(wrapper.find('ComicProfileSection[title="Stories"]').props().data).toEqual(props.comic.stories);
  });

  it('renders ComicProfileSection 3 times', () => {
    const wrapper = setup(props);
    expect(wrapper.find('ComicProfileSection').length).toBe(3);
  })
});
