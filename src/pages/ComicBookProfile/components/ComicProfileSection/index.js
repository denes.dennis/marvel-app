import React        from 'react';
import PropTypes    from 'prop-types';
import { Row, Col } from "../../../../components/external/Grid";

export const ComicProfileSection = ({ title, data }) => {
  return (
    <>
      <Row className="my-3">
        <Col>
          <h1>{ title }: { data.available }</h1>
        </Col>
      </Row>

      <Row className="my-2">
        <Col>
          { displayItems(data.items) }
        </Col>
      </Row>
    </>
  );

};

export const displayItems = (items) => {
  if ( items.length ) {
    return items.map(item => (
      <div key={ Math.random() }>
        <a href={ item.resourceURI }>Resource Link</a>
        <p>{ item.name }</p>
        <p>{ item.type }</p>
      </div>
    ))
  } else {
    return <p>No data found.</p>;
  }
};

ComicProfileSection.propTypes = {
  title: PropTypes.string.isRequired,
  data:  PropTypes.object.isRequired
};
