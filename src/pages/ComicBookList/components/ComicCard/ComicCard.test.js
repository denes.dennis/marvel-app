import React         from 'react'
import { shallow }   from 'enzyme'
import { ComicCard } from './index'

const setup = props => {
  return shallow(<ComicCard {...props} />)
};

const props = {
  id: 432,
  thumbnail: 'https://picsum.photos/200',
  title: 'Test title',
  format: 'comic'
};

describe('ComicCard component tests', () => {
  it('renders without crashing', () => {
    setup(props);
  });

  it('renders correct information', () => {
    const wrapper = setup(props);
    expect(wrapper.find('.card-img-top').props().src).toEqual(props.thumbnail);
    expect(wrapper.find('.card-title').text()).toEqual(props.title);
    expect(wrapper.find('.card-text').text()).toEqual(props.format);
    expect(wrapper.find('Link').first().props().to).toEqual(`/comic/${ props.id }`);
  })
});
