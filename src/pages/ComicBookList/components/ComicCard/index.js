import React      from 'react';
import PropTypes  from 'prop-types';
import { Link }   from 'react-router-dom';
import { Button } from "../../../../components/external/Button";

export const ComicCard = ({ id, thumbnail, title, format }) => {
  return (
    <div className="card">
      <Link to={ `/comic/${ id }` }>
        <img className="card-img-top" src={ thumbnail } height="250px" alt={ title } style={ { objectFit: 'cover' } }/>
      </Link>

      <div className="card-body">
        <h5 className="card-title">{ title }</h5>
        <p className="card-text">{ format }</p>
        <Link to={ `/comic/${ id }` }><Button variant="primary">View details</Button></Link>
      </div>
    </div>
  );
};

ComicCard.propTypes = {
  id:        PropTypes.number.isRequired,
  thumbnail: PropTypes.string.isRequired,
  title:     PropTypes.string.isRequired,
  format:    PropTypes.string.isRequired
};
