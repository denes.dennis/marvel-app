import React         from 'react'
import { shallow }   from 'enzyme'
import { ComicList } from './index'

const setup = props => {
  return shallow(<ComicList { ...props } />)
};

const props = {
  comics: [
    {
      id:        123,
      title:     'X-man',
      format:    'cover',
      thumbnail: {
        path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
        extension: 'jpg'
      }
    },
    {
      id:        321,
      title:     'Antman',
      format:    'comic',
      thumbnail: {
        path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
        extension: 'jpg'
      }
    }
  ]
};

describe('ComicList component tests', () => {
  it('renders without crashing', () => {
    setup(props);
  });

  it('renders ComicCard 2 times', () => {
    const wrapper = setup(props);
    expect(wrapper.find('ComicCard').length).toBe(2);
  });

  it('renders correct information', () => {
    const wrapper = setup(props);
    expect(wrapper.find('ComicCard').first().props().id).toEqual(props.comics[ 0 ].id);
    expect(wrapper.find('ComicCard').first().props().thumbnail).toEqual(`${ props.comics[ 0 ].thumbnail.path }.${ props.comics[ 0 ].thumbnail.extension }`);
    expect(wrapper.find('ComicCard').first().props().title).toEqual(props.comics[ 0 ].title);
    expect(wrapper.find('ComicCard').first().props().format).toEqual(props.comics[ 0 ].format);
  })
});
