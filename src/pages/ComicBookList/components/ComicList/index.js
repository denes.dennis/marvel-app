import React         from 'react';
import PropTypes     from 'prop-types';
import { ComicCard } from "../ComicCard";
import { Col }       from "../../../../components/external/Grid";

export const ComicList = ({ comics }) => {
  return comics && comics.length && comics.map(comic => (
    <Col xs={ 12 } sm={ 6 } md={ 5 } lg={ 4 } xl={ 3 } key={ Math.random() } className="my-2">
      <ComicCard
        id={ comic.id }
        thumbnail={ `${ comic.thumbnail.path }.${ comic.thumbnail.extension }` }
        title={ comic.title }
        format={ comic.format }
        key={ Math.random() }
      />
    </Col>
  ));
};

ComicList.propTypes = {
  comics: PropTypes.array.isRequired
};
