import React         from 'react';
import { connect }   from 'react-redux';
import { Spinner }   from "../../components/Spinner";
import { Alert }     from "../../components/Alert";
import { ComicList } from "./components/ComicList";
import { Row }       from "../../components/external/Grid";

export class ComicBookListClass extends React.Component {
  render() {
    const { comics: { comicsLoading } } = this.props;

    return comicsLoading ? <Spinner/> : this.displayComicsList();
  }

  displayComicsList() {
    const { comics: { comicsError, data } } = this.props;

    if ( data && data.results && data.results.length ) {
      return (
        <Row className="justify-content-center">
          <ComicList comics={ data.results }/>
        </Row>
      );
    } else if ( comicsError ) {
      return <Alert type="danger" title="Error" message={ comicsError }/>;
    } else {
      return <Alert type="warning" title=":(" message="No comic books found."/>;
    }
  }
}

const mapStateToProps = ({ comics }) => {
  return { comics }
};

export const ComicBookList = connect(mapStateToProps)(ComicBookListClass);
