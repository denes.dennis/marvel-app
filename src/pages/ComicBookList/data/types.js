export const GET_COMICS_LOADING = 'get_comics_loading';
export const GET_COMICS_SUCCESS = 'get_comics_success';
export const GET_COMICS_ERROR   = 'get_comics_error';
