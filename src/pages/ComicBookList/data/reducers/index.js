import {
  GET_COMICS_LOADING,
  GET_COMICS_SUCCESS,
  GET_COMICS_ERROR
} from "../types";

const initialState = {
  comicsLoading: false,
  comicsError:   "",
  data:          []
};

export const comics = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_COMICS_LOADING:
      return {
        ...state,
        comicsLoading: true
      };
    case GET_COMICS_SUCCESS:
      return {
        ...state,
        comicsLoading: false,
        data:          payload.data.data
      };
    case GET_COMICS_ERROR:
      return {
        ...state,
        comicsLoading: false,
        comicsError:   payload.data.message
      };
    default:
      return state;
  }
};
