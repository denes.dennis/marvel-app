import { comics } from "./index";
import {
  GET_COMICS_LOADING,
  GET_COMICS_SUCCESS,
  GET_COMICS_ERROR
}                 from "../types";

describe('Comics Reducers', () => {
  it('should return the initial state', () => {
    expect(comics(undefined, {})).toEqual({
      comicsLoading: false,
      comicsError:   "",
      data:          []
    })
  });

  it('should handle GET_COMICS_LOADING', () => {
    expect(
      comics(
        {
          comicsLoading: false,
          comicsError:   "",
          data:          []
        },
        {
          type: GET_COMICS_LOADING
        }
      )
    ).toEqual({
      comicsLoading: true,
      comicsError:   "",
      data:          []
    })
  });

  it('should handle GET_COMICS_ERROR', () => {
    expect(
      comics(
        {
          comicsLoading: false,
          comicsError:   "",
          data:          []
        },
        {
          type:    GET_COMICS_ERROR,
          payload: { data: { message: 'errorOps' } }
        }
      )
    ).toEqual({
      comicsLoading: false,
      comicsError:   "errorOps",
      data:          []
    })
  });

  it('should handle GET_COMICS_SUCCESS', () => {
    expect(
      comics(
        {
          comicsLoading: false,
          comicsError:   "",
          data:          []
        },
        {
          type:    GET_COMICS_SUCCESS,
          payload: {
            data: {
              data:
                [
                  {
                    id:        123,
                    title:     'X-man',
                    format:    'cover',
                    thumbnail: {
                      path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
                      extension: 'jpg'
                    }
                  },
                  {
                    id:        321,
                    title:     'Antman',
                    format:    'comic',
                    thumbnail: {
                      path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
                      extension: 'jpg'
                    }
                  }
                ]
            }
          }
        }
      )
    ).toEqual({
      comicsLoading: false,
      comicsError:   "",
      data:          [
        {
          id:        123,
          title:     'X-man',
          format:    'cover',
          thumbnail: {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          }
        },
        {
          id:        321,
          title:     'Antman',
          format:    'comic',
          thumbnail: {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          }
        }
      ]
    })
  });
});
