import {
  getComicsLoading,
  getComicsSuccess,
  getComicsError
} from './comics';
import {
  GET_COMICS_LOADING,
  GET_COMICS_SUCCESS,
  GET_COMICS_ERROR
} from "../types";

describe('Comics Actions', () => {
  it('should create a GET_COMICS_LOADING action', () => {
    const expectedAction = { type: GET_COMICS_LOADING };
    const action         = getComicsLoading();
    expect(action).toEqual(expectedAction);
  });

  it('should create a GET_COMICS_ERROR action with error', () => {
    const expectedAction = { type: GET_COMICS_ERROR, payload: 'error' };
    const action         = getComicsError('error');
    expect(action).toEqual(expectedAction);
  });

  it('should create a GET_COMICS_SUCCESS action', () => {
    const comics = [
      {
        id:        123,
        title:     'X-man',
        format:    'cover',
        thumbnail: {
          path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg'
        }
      },
      {
        id:        321,
        title:     'Antman',
        format:    'comic',
        thumbnail: {
          path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg'
        }
      }
    ];

    const expectedAction = {
      type:    GET_COMICS_SUCCESS,
      payload: [
        {
          id:        123,
          title:     'X-man',
          format:    'cover',
          thumbnail: {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          }
        },
        {
          id:        321,
          title:     'Antman',
          format:    'comic',
          thumbnail: {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          }
        }
      ]
    };
    const action         = getComicsSuccess(comics);
    expect(action).toEqual(expectedAction);
  })
});
