import { marvelAxios } from "../../../../api/config";
import { MyError }     from "../../../../utils/error";
import {
  GET_COMICS_LOADING,
  GET_COMICS_SUCCESS,
  GET_COMICS_ERROR
} from "../types";

export const getComicsLoading = () => {
  return {
    type: GET_COMICS_LOADING
  }
};

export const getComicsSuccess = (comics) => {
  return {
    type:    GET_COMICS_SUCCESS,
    payload: comics
  }
};

export const getComicsError = (error) => {
  return {
    type:    GET_COMICS_ERROR,
    payload: error
  }
};

export const getComics = (params) => {
  return async (dispatch) => {
    try {
      dispatch(getComicsLoading());
      const res = await marvelAxios.get('/comics', { params });
      dispatch(getComicsSuccess(res));
    } catch (error) {
      dispatch(getComicsError(error.response));
      MyError(error);
    }
  };
};
