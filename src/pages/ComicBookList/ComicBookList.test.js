import React             from 'react';
import { shallow }       from 'enzyme';
import { ComicBookList } from './index';

const props = {
  comics: {
    data: {
      results: [
        {
          id:        123,
          title:     'X-man',
          format:    'cover',
          thumbnail: {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          }
        },
        {
          id:        321,
          title:     'Antman',
          format:    'comic',
          thumbnail: {
            path:      'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
            extension: 'jpg'
          }
        }
      ]
    }
  }
};

const props2 = {
  comics: {
    data: {},
    comicsError: 'BlaBla'
  }
};

const props3 = {
  comics: {}
};

const setup = props => {
  return shallow(<ComicBookList.WrappedComponent { ...props } />);
};

describe('ComicBookList component tests', () => {
  it('renders without crashing', () => {
    setup(props);
  });

  it('renders correct information', () => {
    const wrapper = setup(props);
    expect(wrapper.find('ComicList').length).toBe(1);
    expect(wrapper.find('ComicList').props().comics).toEqual(props.comics.data.results);
  });

  it('displays error if there is an error fetching comics', () => {
    const wrapper = setup(props2);
    expect(wrapper.find('Alert').length).toBe(1);
    expect(wrapper.find('Alert').props().message).toEqual(props2.comics.comicsError);
  });

  it('displays warning if there are no comics', () => {
    const wrapper = setup(props3);
    expect(wrapper.find('Alert').length).toBe(1);
    expect(wrapper.find('Alert').props().message).toEqual('No comic books found.');
  })
});
